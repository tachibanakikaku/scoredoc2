# -*- coding: utf-8 -*-
class TournamentHtmlFormatter
  def initialize(participants)
    @participants = participants.to_a
    matches = MatchMaker.new(@participants.count)
    @grid = Grid.new(matches)
  end

  def output
    arr = []
    first = true
    @grid.data.reverse_each do |row|
      if(first)
        arr = participant_tags(arr, row)
        first = false
        next
      end

      arr = line_tags(arr, row)
    end
    arr.join
  end

  def participant_tags(arr, row)
    arr.push('<div class="participants">')
    row.each do |cell|
      if(cell == '  ')
        arr.push('<div class="participants_gap"></div>')
      else
        participant = @participants.shift
        arr.push('<div class="participant">')
        arr.push('  <input type="text" size="3" ')
        arr.push('         value="' + participant.name.to_s + '"')
        arr.push('         data-participant_id=' + participant.id.to_s)
        arr.push('  ></input>')
        arr.push('</div>')
      end
    end
    arr.push('</div>')
    return arr
  end

  def line_tags(arr, row)
    arr.push('<div class="unit_vertical_container">')
    row.each do |cell|
      arr.push('<div class="unit_container">')
      case cell
      when '| '
        arr.push('<canvas height="60px" width="200px" class="unit horizontal_slide"></canvas>')
      when '|-'
        arr.push('<canvas height="60px" width="200px" class="unit downer_hook"></canvas>')
      when '-|'
        arr.push('<canvas height="60px" width="200px" class="unit upper_hook"></canvas>')
      when '⊥ '
        arr.push('<canvas height="60px" width="200px" class="unit cap"></canvas>')
      when '--'
        arr.push('<canvas height="60px" width="200px" class="unit vertical_slide"></canvas>')
      end
      arr.push('</div>')
    end
    arr.push('</div>')

    return arr
  end
end
