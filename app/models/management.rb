class Management < ActiveRecord::Base
  belongs_to :manager, class_name: :User, foreign_key: :user_id
  belongs_to :managed_tournament, class_name: :Tournament, foreign_key: :tournament_id

  def owner?
    role == 'owner'
  end

  def collaborator
  end
end
