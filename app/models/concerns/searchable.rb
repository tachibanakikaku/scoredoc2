module Searchable
  extend ActiveSupport::Concern

  module ClassMethods
    def search(term = nil)
      where('name LIKE ?', "%#{term}%")
    end
  end
end
