# -*- coding: utf-8 -*-
class MatchMaker
  attr_reader :matches, :smallest_power, :num_entries, :seeds, :temp_match_no, :match_no

  def initialize(num_entries)
    @num_entries = num_entries
    @match_no = 0
    @temp_match_no = 0
    @smallest_power = 2 ** Math.log(num_entries,2).ceil
    @seeds = find_seeds()
    @matches = match_make(@smallest_power)
  end

  def match_make(num_entries)
    matches = break_down(num_entries)

    if(matches[0] > 1)
      matches[0] = match_make(matches[0])
    end

    if(matches[1] > 1)
      matches[1] = match_make(matches[1])
    end

    return matches
  end

  def break_down(num)
    left = (num / 2).floor
    if (left == 1)
      @temp_match_no += 1
      if(@seeds.include?(temp_match_no))
        return [1,0,0]
      end
    end
    @match_no += 1
    return [left,num - left,@match_no]
  end

  def find_seeds
    round = Math.log(@smallest_power/2,2).ceil
    seeds = [1,2]
    (0...round).each do
      seeds = next_mountain(seeds)
    end

    return seeds.slice(0,(@smallest_power - @num_entries))
  end

  def next_mountain(seeds)
    arr = []
    length = seeds.length + 1;
    seeds.each do |seed|
      arr.push(seed)
      arr.push(length-seed)
    end
    return arr;
  end
end
