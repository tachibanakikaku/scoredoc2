class Highlight < ActiveRecord::Base
  include Authority::Abilities

  belongs_to :game
end
