class Tournament < ActiveRecord::Base
  include Authority::Abilities
  include Searchable

  has_many :games
  has_many :managements, dependent: :destroy
  has_many :participations, dependent: :destroy
  has_many :managers, through: :managements
  has_many :participants, through: :participations

  scope :published, -> { where(published: true) }

  validates :name, presence: true

  DEFAULT_PARTICIPANT_COUNT = 4
  DEFAULT_GAEME_COUNT = DEFAULT_PARTICIPANT_COUNT - 1

  def self.create_with_content(attr)
    attr[:uuid] = SecureRandom.uuid
    @tournament = Tournament.new(attr)

    DEFAULT_PARTICIPANT_COUNT.times { @tournament.participants.build }
    DEFAULT_GAEME_COUNT.times { @tournament.games.build }

    @tournament.tap(&:save)
  end

  def staff?(user)
    m = managements.where(user_id: user).first
    m.nil? ? false : %w(owner collaborator).include?(m.role)
  end

  def owner?(user)
    m = managements.where(user_id: user).first
    m.nil? ? false : (m.role == 'owner')
  end
end
