class User < ActiveRecord::Base
  include Authority::UserAbilities

  has_many :managements
  has_many :participations
  has_many :managed_tournaments, through: :managements
  has_many :tournaments, through: :participations

  def self.find_or_create_from_auth_hash(auth_hash)
    provider = auth_hash[:provider]
    uid = auth_hash[:uid]
    nickname = auth_hash[:info][:nickname]
    name = auth_hash[:info][:name]
    location = auth_hash[:info][:location]
    image_url = auth_hash[:info][:image]

    User.find_or_create_by(provider: provider, uid: uid) do |user|
      user.nickname = nickname
      user.name = name
      user.location = location
      user.image_url = image_url
    end
  end
end
