class Result < ActiveRecord::Base
  include Authority::Abilities

  belongs_to :game
end
