class Participation < ActiveRecord::Base
  belongs_to :participant, class_name: :User, foreign_key: :user_id
  belongs_to :tournament

  def tournament_id
    tournament.id
  end
end
