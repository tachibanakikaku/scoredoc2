class Game < ActiveRecord::Base
  include Authority::Abilities

  belongs_to :tournament
  has_one :result
  has_many :highlights
end
