$(document).ready(function () {
    $('#root-wizard').bootstrapWizard({
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current/$total) * 100;
            $('#root-wizard')
                .find('.bar')
                .text(Math.floor($percent) + '%')
                .css({ width: $percent + '%' });
            if($current == $total){
                $('.finish').show();
            }else{
                $('.finish').hide();
            }
        }
    });
});
