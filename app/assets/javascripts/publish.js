$(document).ready(function(){
  $("#form-values").hide();
  is_checked = $("#tournament_published").prop("checked");
  $("#tournament_published").prop("checked",!is_checked);
  if (is_checked){
    $("#btn-publish").text("Unpublish Tournament");
  }else{
    $("#btn-publish").text("Publish Tournament");
  }
});