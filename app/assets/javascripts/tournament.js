$(document).ready(function() {   
    draw_tournament();
    
    bind_participant_name_update();
});


function draw_tournament(){
    // can't set css to canvas, so set attributes
    $('.unit').attr( 'width', $('.unit_container').css('width') );
    $('.unit').attr( 'height', $('.unit_container').css('height') );
 
    // elliminate 'px'
    var unit_width  = parseInt( $('.unit').attr('width') );
    var unit_height = parseInt( $('.unit').attr('height') ); 

    // set the writing line points
    var left_middle       = [ 0                , unit_height * 0.5 ];
    var half_right_middle = [ unit_width * 0.75, unit_height * 0.5 ];
    var half_right_bottom = [ unit_width * 0.75, unit_height       ];
    var half_right_top    = [ unit_width * 0.75, 0                 ];
    var right_middle      = [ unit_width       , unit_height * 0.5 ];
 
    var draw_style = {
      strokeStyle: '#555',
      strokeWidth: 1,
      rounded: true
    };

    draw_line('.vertical_slide', 
		draw_style,  
		[ half_right_top    ,
		  half_right_bottom ]
              );

    draw_line('.horizontal_slide', 
		draw_style,  
		[ left_middle       ,
		  right_middle      ]
              );

    draw_line('.downer_hook', 
		draw_style,  
		[ left_middle       ,
		  half_right_middle ,
		  half_right_bottom ]
              );

    draw_line('.upper_hook', 
		draw_style,  
		[ left_middle       ,
		  half_right_middle ,
		  half_right_top    ]
	      );

    draw_line('.cap',        
		draw_style,  
		[ half_right_top    ,
		  half_right_bottom ]
	      ).draw_line('.cap',        
		draw_style,  
		[ half_right_middle ,
		  right_middle      ]		
	      );    
}

function draw_line(selector, style, points) {

    var style_and_points = $.extend(true, {}, style);;

    for (var p=0; p<points.length; p+=1) {
        style_and_points['x'+(p+1)] = points[p][0];
        style_and_points['y'+(p+1)] = points[p][1];
    }

    $(selector).drawLine(style_and_points);

    return this;
}

function bind_participant_name_update(){
    var token = $( 'meta[name="csrf-token"]' ).attr( 'content' );

    $.ajaxSetup( {
        beforeSend: function ( xhr ) {
            xhr.setRequestHeader( 'X-CSRF-Token', token );
        }
    });      
    
    $('.participant input').blur( function() {        
        $.ajax({
            url: '/participants/' + $(this).attr('data-participant_id'),
            type: 'PATCH',
            data: {participant: {name: $(this).val() }},
            dataType: 'html',
            error: function(data) {
                alert(data.statusText);
            }
        });
    });        
}
