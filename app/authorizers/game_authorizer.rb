class GameAuthorizer < Authority::Authorizer
  def self.creatable_by?(user)
    resource.tournament.managers.include?(user)
  end

  def updatable_by?(user)
    resource.tournament.updatable_by?(user)
  end

  def deletable_by?(user)
    resource.tournament.managers.include?(user)
  end
end
