class TournamentAuthorizer < Authority::Authorizer
  def self.creatable_by?(_user)
    true # login_check is done in controller.
  end

  def updatable_by?(user)
    resource.staff?(user)
  end

  def deletable_by?(user)
    resource.owner?(user)
  end
end
