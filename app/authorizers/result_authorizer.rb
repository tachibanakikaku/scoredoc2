class ResultAuthorizer < Authority::Authorizer
  def self.creatable_by?(user)
    resource.game.creatable_by?(user)
  end

  def updatable_by?(user)
    resource.game.updatable_by?(user)
  end

  def deletable_by?(user)
    resource.game.deletable_by?(user)
  end
end
