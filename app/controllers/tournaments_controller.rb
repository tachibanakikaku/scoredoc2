class TournamentsController < ApplicationController
  before_action :set_tournament, only: [:show, :edit, :update, :destroy]
  before_action :can_create?, only: [:new, :create]
  before_action only: [:edit, :update] { can_update?(@tournament) }
  before_action only: [:destroy] { can_delete?(@tournament) }

  def new
    @tournament = Tournament.new
  end

  def edit
  end

  def create
    management = Management.new(manager: current_user, role: 'owner')
    @tournament = Tournament.new(tournament_params.merge(managements: [management]))

    respond_to do |format|
      if @tournament.save
        format.html { redirect_to @tournament, notice: t('view.create_success') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    @tournament.update(tournament_params)
    redirect_to tournament_path(@tournament)
  end

  def add_participation
    tournament = Tournament.find(params[:id])
    participation = tournament.participations.build

    participation.save
    redirect_to tournament_tournament_path(tournament)
  end

  def destroy
    @tournament.destroy
    redirect_to :root, notice: t('view.destroy_success')
  end

  private

  def set_tournament
    @tournament = Tournament.find(params[:id])
  end

  def tournament_params
    params.require(:tournament).permit(:name, :description, :opened_at, :published)
  end
end
