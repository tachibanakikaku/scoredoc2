class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception
  helper_method :logged_in?, :current_user
  around_filter :time_zone

  def time_zone(&block)
    time_zone = current_user.try(:time_zone) || 'UTC'
    Time.use_zone(time_zone, &block)
  end

  def check_login
    return if logged_in?
    not_allowed('view.please_login')
  end

  def can_create?
    check_login
  end

  def can_update?(instance)
    check_login
    not_allowed unless current_user.can_update?(instance)
  end

  def can_delete?(instance)
    check_login
    not_allowed unless current_user.can_delete?(instance)
  end

  private

  def not_allowed(key = 'view.not_allowed')
    flash[:error] = t(key)
    redirect_to :root
  end
end
