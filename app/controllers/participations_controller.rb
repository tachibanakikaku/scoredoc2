class ParticipationsController < ApplicationController
  before_action :set_participation, only: [:edit, :update, :destroy]
  before_action :set_tournament, only: [:index]
  before_action :set_users, only: [:new]
  before_action :check_login, only: [:new, :create, :edit, :update, :destroy]

  def index
    @participations = Participation.where(tournament_id: params[:t_id])
  end

  def new
    @participation = Participation.new(tournament_id: params[:t_id])

    render 'new'
  end

  def create
    @participation = Participation.new(participation_params)

    unless @participation.save
      render 'new'
      return
    end

    redirect_to participations_path(t_id: @participation.tournament_id), notice: t('view.create_success')
  end

  def edit
  end

  def update
    unless @participation.update(participation_params)
      render 'edit'
      return
    end

    redirect_to participations_url(t_id: @participation.tournament_id), notice: t('view.update_success')
  end

  def destroy
    @participation.destroy

    redirect_to participations_url(t_id: @participation.tournament_id), notice: t('view.destroy_success')
  end

  private

  def set_users
    @users = User.all
  end

  def set_tournament
    @tournament = Tournament.find(params[:t_id])
  end

  def set_participation
    @participation = Participation.find(params[:id])
  end

  def participation_params
    params.require(:participation).permit(:name, :user_id, :tournament_id)
  end
end
