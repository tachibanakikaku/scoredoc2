class UsersController < ApplicationController
  before_action :check_login, only: [:show, :edit]

  def edit
    @user = User.find(params[:id])
  end

  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html
    end
  end

  def update
    @user = User.find(params[:id])

    @user.update(user_params)
    redirect_to user_path(@user)
  end

  private

  def user_params
    params.require(:user).permit(:name, :image_url, :time_zone)
  end
end
