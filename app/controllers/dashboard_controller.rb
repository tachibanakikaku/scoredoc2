class DashboardController < ApplicationController
  def index
    @tournaments = Tournament.published.search(params[:term])
  end
end
