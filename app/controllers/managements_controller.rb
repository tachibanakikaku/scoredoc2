class ManagementsController < ApplicationController
  before_action :set_tournament, only: [:index]
  before_action :set_management, only: [:destroy]

  def index
    @managements = Management.where(tournament_id: params[:t_id])
  end

  def new
    @management = Management.new(tournament_id: params[:t_id])

    render 'new'
  end

  def create
    @management = Management.new(management_params)

    unless @management.save
      render 'new'
      return
    end

    redirect_to managements_url(t_id: @management.tournament_id), notice: t('view.create_success')
  end

  def destroy
    @management.destroy

    redirect_to managements_url(t_id: @management.tournament_id), notice: t('view.destroy_success')
  end

  private

  def set_tournament
    @tournament = Tournament.find(params[:t_id])
  end

  def set_management
    @management = Management.find(params[:id])
  end

  def management_params
    params.require(:management).permit(:role, :user_id, :tournament_id)
  end
end
