module ApplicationHelper
  def logged_in?
    !session[:user_id].nil?
  end

  def current_user
    @current_user ||= User.where(session[:user_id]).first
  end
end
