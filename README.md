# Scoredoc2

[![wercker status](https://app.wercker.com/status/308daf94d482b7abc9be82efe718b811/m "wercker status")](https://app.wercker.com/project/bykey/308daf94d482b7abc9be82efe718b811)

[![Coverage Status](https://coveralls.io/repos/bitbucket/tachibanakikaku/scoredoc2/badge.svg?branch=HEAD)](https://coveralls.io/bitbucket/tachibanakikaku/scoredoc2?branch=HEAD)

# README

## For Developer

##### Languages

- Go
- Node.js
- Ruby

##### Libraries

- direnv (Go)

  - copy `.envrc.sample` to `.envrc`.
  - update values for your twitter application.

- bower (Node.js)

  - `bower install` to get some libraries

and do `bundle install` as usual for Ruby.

##### Local Server

`bundle exec unicorn`
