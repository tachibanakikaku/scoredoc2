class RemoveImageFromHighlight < ActiveRecord::Migration
  def change
    remove_column :highlights, :image, :binary
  end
end
