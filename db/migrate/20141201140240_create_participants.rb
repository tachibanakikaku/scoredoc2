class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.integer :code
      t.integer :ranking
      t.integer :user_id
      t.integer :tournament_id

      t.timestamps
    end
  end
end
