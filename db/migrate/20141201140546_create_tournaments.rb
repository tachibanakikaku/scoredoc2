class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.string :name
      t.string :description
      t.string :uuid
      t.datetime :opened_at

      t.timestamps
    end
  end
end
