class CreateHighlights < ActiveRecord::Migration
  def change
    create_table :highlights do |t|
      t.integer :game_id
      t.string :description
      t.binary :image

      t.timestamps
    end
  end
end
