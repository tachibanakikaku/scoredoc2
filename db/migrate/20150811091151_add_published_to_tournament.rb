class AddPublishedToTournament < ActiveRecord::Migration
  def change
    add_column :tournaments, :published, :bool, default: false
  end
end
