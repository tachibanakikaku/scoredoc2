class RenameManagersToManagements < ActiveRecord::Migration
  def change
    rename_table :managers, :managements
  end
end
