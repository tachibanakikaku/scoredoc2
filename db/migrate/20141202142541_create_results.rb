class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :game_id
      t.integer :winner_id
      t.integer :score_a
      t.integer :score_b
      t.string :description

      t.timestamps
    end
  end
end
