class CreateManagers < ActiveRecord::Migration
  def change
    create_table :managers do |t|
      t.integer :user_id
      t.integer :tournament_id
      t.string :role

      t.timestamps
    end
  end
end
