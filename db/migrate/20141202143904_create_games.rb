class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :tournament_id
      t.string :display_code
      t.integer :player_a_id
      t.integer :player_b_id
      t.datetime :held_at

      t.timestamps
    end
    add_index :games, :display_code, unique: true
  end
end
