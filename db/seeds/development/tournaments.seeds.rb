us_opens = (2003..2014).map do |y|
  {
    name: "U.S.Open Championships #{y}",
    description: 'Tennis Tournaments',
    uuid: SecureRandom.uuid,
    opened_at: "#{y}/08/28 10:00:00",
    published: true,
    created_at: Time.now,
    updated_at: Time.now,
  }
end

worldcups = (1998..2014).step(4).map do |y|
  {
    name: "FIFA World Cup #{y}",
    description: 'World Cup',
    uuid: SecureRandom.uuid,
    opened_at: "#{y}/06/12 09:00:00",
    published: true,
    created_at: Time.now,
    updated_at: Time.now
  }
end

olympics = (1996..2012).step(4).map do |y|
  {
    name: "Olympic #{y}",
    description: 'Olympic',
    uuid: SecureRandom.uuid,
    opened_at: "#{y}/07/21 08:00:00",
    published: true,
    created_at: Time.now,
    updated_at: Time.now
  }
end

tournaments = [us_opens, worldcups, olympics].flatten
Tournament.create(tournaments)
