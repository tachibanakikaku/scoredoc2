# -*- coding: utf-8 -*-
require './match_maker.rb'
require './grid.rb'

class ConsoleFormatter
  def initialize(num)
    matches = MatchMaker.new(num)
    @grid = Grid.new(matches)
  end

  def output
    rows = []
    @grid.data.each do |row|
      rows.push(row)
    end
    rows
  end
end
