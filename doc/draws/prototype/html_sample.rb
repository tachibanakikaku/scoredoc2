require './html_formatter.rb'
require 'cgi'

File.delete 'canvas.html' if File.exist?('canvas.html')
p 'deleted canvas.html'

formatter = HtmlFormatter.new(10)
tournament_tags = formatter.output

html_source = open('canvas_template.html', 'r').read
html_source = html_source.gsub('<<<contents>>>',tournament_tags)
html_source = CGI.pretty(html_source)

dest = open('canvas.html', 'w')
dest.write(html_source)
p 'wrote canvas.html'
