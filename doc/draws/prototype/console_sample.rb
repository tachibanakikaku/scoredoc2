# -*- coding: utf-8 -*-
require './console_formatter.rb'

formatter = ConsoleFormatter.new(10)
rows = formatter.output
rows.each { |row| p row }
