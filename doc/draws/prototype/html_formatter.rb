# -*- coding: utf-8 -*-
require './match_maker.rb'
require './grid.rb'

class HtmlFormatter
  def initialize(num)
    matches = MatchMaker.new(num)
    @grid = Grid.new(matches)
  end

  def output
    arr = []
    first = true
    @grid.data.reverse_each do |row|
      if(first)
        arr.push('<div class="participants">')
        row.each do |cell|
          if(cell == '  ')
            arr.push('<div class="participants_gap"></div>')
          else
            arr.push('<div class="participant">' + cell + '</div>')
          end
        end
        arr.push('</div>')
        first = false
        next
      end

      arr.push('<div class="unit_vertical_container">')
      row.each do |cell|
        arr.push('<div class="unit_container">')
        case cell
        when '| '
          arr.push('<canvas height="30px" width="100px" class="unit horizontal_slide"></canvas>')
        when '|-'
          arr.push('<canvas height="30px" width="100px" class="unit downer_hook"></canvas>')
        when '-|'
          arr.push('<canvas height="30px" width="100px" class="unit upper_hook"></canvas>')
        when '⊥ '
          arr.push('<canvas height="30px" width="100px" class="unit cap"></canvas>')
        when '--'
          arr.push('<canvas height="30px" width="100px" class="unit vertical_slide"></canvas>')
        end
        arr.push('</div>')
      end
      arr.push('</div>')
    end
    arr.join
  end
end
