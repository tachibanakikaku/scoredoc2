# -*- coding: utf-8 -*-
class Grid
  attr_reader :data,:width,:max_height

  def initialize(match_maker)
    @max_height = Math.log(match_maker.smallest_power,2).ceil
    @width = match_maker.smallest_power * 2 - 1
    grid = Array.new(@max_height + 1) { Array.new(@width,'  ') }
    @data = embed(grid, match_maker.matches)
  end

  def embed(grid, matches, height = 0 ,base_left = 0)
    grid = embed_cap(grid, matches, height, base_left)
    height += 1

    if matches[0].is_a?(Array)
      embed(grid, matches[0], height , base_left)
    end

    if matches[1].is_a?(Array)
      base_left += (matches.flatten.size / 2 + 1)
      embed(grid, matches[1], height , base_left)
    end

    if(height == @max_height)
      num = 0
      (0..(@width - 1)).each do |left|
        if(grid[height - 1][left] =~ /(\| |\|-|-\|)/)
          num += 1
          grid[height][left] = format('%02d', num.to_s)
        end
      end
    end

    return grid
  end

  def embed_cap(grid, matches, height, base_left)
    left  = matches[0].is_a?(Array) ?  (matches[0].flatten.size / 2) : 0
    left += base_left
    right = (matches.flatten.size / 2 + 1) + left

    if(matches[1] == 0)
      grid[height][left + 1] = '| '
      return grid
    end

    grid[height][left  ] = '|-'
    grid[height][right ] = '-|'
    center = right - left - 1 + base_left

    ((left+1)..(right-1)).each do |idx|
      if(idx == center)
        grid[height][idx] = '⊥ '
      else
        grid[height][idx] = '--'
      end
    end

    return grid
  end
end
