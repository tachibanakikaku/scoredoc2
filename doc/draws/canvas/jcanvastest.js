function write_line(obj, pts) {
    for (var p=0; p<pts.length; p+=1) {
        obj['x'+(p+1)] = pts[p][0];
        obj['y'+(p+1)] = pts[p][1];
    }
    $('canvas').drawLine(obj);
}

$(document).ready(function() {
    var obj = {
      strokeStyle: '#000',
      strokeWidth: 6,
      rounded: true
    };

    write_line(obj,  [
        [50, 0],
        [100, 0],
        [100, 50]
    ]);

    write_line(obj,  [
        [50, 100],
        [100, 100],
        [100, 50]
    ]);

});
