Rails.application.routes.draw do
  # login / logout
  get '/auth/:provider/callback', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy', as: :logout

  resources :tournaments, except: :index do
    resources :participants
  end

  resources :managements
  resources :participations

  resources :users, only: [:show, :edit, :update]

  root 'dashboard#index'
end
