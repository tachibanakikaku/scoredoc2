require 'rails_helper'

RSpec.describe Highlight, type: :authorizer do
  describe 'instance' do
    context 'user is owner' do
      let(:owner)     { create(:management, :owner) }
      let(:user)      { owner.manager }
      let(:game)      { create(:game, tournament: owner.managed_tournament) }
      let(:highlight) { create(:highlight, game: game) }

      it 'allow to update' do
        expect(highlight).to be_updatable_by(user)
      end

      it 'allow to delete' do
        expect(highlight).to be_deletable_by(user)
      end
    end

    context 'user is collaborator' do
      let(:collaborator) { create(:management, :collaborator) }
      let(:user)         { collaborator.manager }
      let(:game)         { create(:game, tournament: collaborator.managed_tournament) }
      let(:highlight)    { create(:highlight, game: game) }

      it 'allow collaborator to update' do
        expect(highlight).to be_updatable_by(user)
      end

      it 'allow collaborator to delete' do
        expect(highlight).to be_deletable_by(user)
      end
    end
  end
end
