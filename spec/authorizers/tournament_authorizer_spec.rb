require 'rails_helper'

RSpec.describe Tournament, type: :authorizer do
  describe 'instance' do
    context 'when user is not owner nor collaborator' do
      let(:tournament) { create(:tournament) }
      let(:user)       { create(:user) }

      it 'allows to create' do
        expect(Tournament).to be_creatable_by(user)
      end

      it 'does not allow to update' do
        expect(tournament).not_to be_updatable_by(user)
      end

      it 'does not allow to delete' do
        expect(tournament).not_to be_deletable_by(user)
      end
    end

    context 'when user is owner' do
      let(:owner)      { create(:management, :owner, manager: user) }
      let(:tournament) { owner.managed_tournament }
      let(:user)       { create(:user) }

      it 'allows to update' do
        expect(tournament).to be_updatable_by(user)
      end

      it 'allows to delete' do
        expect(tournament).to be_deletable_by(user)
      end
    end

    context 'when user is collaborator' do
      let(:collaborator) { create(:management, :collaborator, manager: user) }
      let(:tournament)   { collaborator.managed_tournament }
      let(:user)         { create(:user) }

      it 'allows to update' do
        expect(tournament).to be_updatable_by(user)
      end

      it 'does not allow to delete' do
        expect(tournament).not_to be_deletable_by(user)
      end
    end
  end
end
