require 'rails_helper'

RSpec.describe Game, type: :authorizer do
  describe 'instance' do
    context 'user is owner' do
      let(:owner)        { create(:management, :owner) }
      let(:game)         { create(:game, tournament: tournament) }
      let(:tournament)   { owner.managed_tournament }
      let(:user)         { owner.manager }

      it 'allow to update' do
        expect(game).to be_updatable_by(user)
      end

      it 'allow to delete' do
        expect(game).to be_deletable_by(user)
      end
    end

    context 'user is collaborator' do
      let(:collaborator) { create(:management, :collaborator) }
      let(:game)         { create(:game, tournament: tournament) }
      let(:tournament)   { collaborator.managed_tournament }
      let(:user)         { collaborator.manager }

      it 'allow collaborator to update' do
        expect(game).to be_updatable_by(user)
      end

      it 'allow collaborator to delete' do
        expect(game).to be_deletable_by(user)
      end
    end
  end
end
