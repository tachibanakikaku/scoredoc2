require 'rails_helper'

RSpec.describe Participation, type: :model do
  it { should belong_to(:participant) }
  it { should belong_to(:tournament) }

  subject(:instance) { Participation.new }

  it { expect(instance).to be_an_instance_of Participation }
end
