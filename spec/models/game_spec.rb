require 'rails_helper'

RSpec.describe Game, type: :model do
  it { should belong_to(:tournament) }
  it { should have_one(:result) }
  it { should have_many(:highlights) }

  subject(:instance) { Game.new }

  it { expect(instance).to be_an_instance_of Game }
end
