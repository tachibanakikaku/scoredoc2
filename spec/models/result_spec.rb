require 'rails_helper'

RSpec.describe Result, type: :model do
  it { should belong_to(:game) }

  subject(:instance) { Result.new }

  it { expect(instance).to be_an_instance_of Result }
end
