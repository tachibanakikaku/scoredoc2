require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:tournaments).through(:participations) }

  subject(:instance) { User.new }

  it { expect(instance).to be_an_instance_of User }

  describe '.find_or_create_from_auth_hash' do
    let(:auth_hash) do
      {
        provider: 'twitter',
        uid: '12345',
        info: {
          nickname: 'taro',
          name: 'YAMADA Taro',
          location: 'Japan',
          image_url: 'https://example.com/example.png'
        }
      }
    end

    context 'when user is already in databse' do
      let!(:existing) { User.find_or_create_from_auth_hash(auth_hash) }

      it 'does not create new record' do
        expect { User.find_or_create_from_auth_hash(auth_hash) }
          .to change(User, :count).by(0)
      end
    end

    context 'when user does not exist in database' do
      it 'creates new record' do
        expect { User.find_or_create_from_auth_hash(auth_hash) }
          .to change(User, :count).by(1)
      end
    end
  end
end
