require 'rails_helper'

RSpec.describe Tournament, type: :model do
  it { should have_many(:participants).through(:participations) }
  it { should have_many(:games) }
  it { expect(Tournament).to include Searchable }

  it_behaves_like 'searchable' do
    let!(:t1)     { create(:tournament, name: 'abc') }
    let!(:t2)     { create(:tournament, name: 'bca') }
    let!(:t3)     { create(:tournament, name: 'cab') }
    let(:term)    { 'a' }
    let(:hit_num) { 3 }
  end

  subject(:instance) { Tournament.new }

  it { expect(instance).to be_an_instance_of Tournament }
end
