require 'rails_helper'

RSpec.describe Highlight, type: :model do
  it { should belong_to(:game) }

  subject(:instance) { Highlight.new }

  it { expect(instance).to be_an_instance_of Highlight }
end
