require 'rails_helper'

RSpec.describe Management, type: :model do
  it { should belong_to(:manager) }
  it { should belong_to(:managed_tournament) }

  subject(:instance) { Management.new }

  it { expect(instance).to be_an_instance_of Management }
end
