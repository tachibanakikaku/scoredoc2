module I18nMacros
  def t(*args)
    I18n.translate!(*args)
  end

  def t_model(klass, attr)
    klass.human_attribute_name(attr)
  end
end
