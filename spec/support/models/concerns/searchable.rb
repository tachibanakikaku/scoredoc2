require 'rails_helper'

shared_examples 'searchable' do
  describe '.search' do
    it { expect(described_class.search(term).size).to eq hit_num }
  end
end
