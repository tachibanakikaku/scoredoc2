FactoryGirl.define do
  factory :role do
    trait :owner do
      name 'owner'
    end

    trait :collaborator do
      name 'collaborator'
    end
  end
end
