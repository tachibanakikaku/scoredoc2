FactoryGirl.define do
  factory :tournament do
    name        'Example Tournament'
    description 'This is an example tournament.'
    uuid        SecureRandom.uuid
    opened_at   '2015-12-06 09:30:00'

    trait :unpublished do
      name      'Unpublished Tournament'
      published false
    end

    trait :published do
      name      'Published Tournament'
      published false
    end
  end
end
