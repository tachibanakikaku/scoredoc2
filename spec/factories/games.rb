FactoryGirl.define do
  factory :game do
    display_code 'No.1 game'
    player_a_id  1
    player_b_id  2
    held_at      '2015-12-06 09:50:00'

    association  :tournament, factory: :tournament
  end
end
