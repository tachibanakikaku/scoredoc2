FactoryGirl.define do
  factory :user do
    sequence(:provider) { |i| "twitter_#{i}" }
    sequence(:uid)      { |i| i }
    nickname  'taro'
    name      'YAMADA Taro'
    location  'Japan'
    image_url 'https://example.com/example.png'

    trait :tokyo do
      time_zone 'Tokyo'
    end

    trait :no_tz do
      time_zone ''
    end
  end
end
