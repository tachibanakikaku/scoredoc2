FactoryGirl.define do
  factory :management do
    association :manager, factory: :user
    association :managed_tournament, factory: :tournament

    trait :owner do
      role 'owner'
    end

    trait :collaborator do
      role 'collaborator'
    end
  end
end
