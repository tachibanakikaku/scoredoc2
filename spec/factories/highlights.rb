FactoryGirl.define do
  factory :highlight do
    description 'Highlight of the game.'

    association :game, factory: :game
  end
end
