FactoryGirl.define do
  factory :participation do
    name 'taro'

    association :participant, factory: :user
    association :tournament, factory: :tournament
  end
end
