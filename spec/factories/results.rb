FactoryGirl.define do
  factory :result do
    winner_id 1
    score_a 5
    score_b 3
    description 'winner is a.'

    association :game, factory: :game
  end
end
