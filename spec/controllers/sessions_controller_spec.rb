require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe 'GET create' do
    let(:user) { create(:user) }

    it 'sets session[:user_id] value' do
      allow(User).to receive(:find_or_create_from_auth_hash).and_return(user)

      get :create, provider: 'twitter'

      expect(session[:user_id]).to eq user.id
    end
  end
end
