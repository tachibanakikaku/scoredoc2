require 'spec_helper'
require 'rails_helper'

RSpec.describe TournamentsController, type: :controller do
  let(:user) { create(:user) }

  describe 'GET new' do
    before(:each) { login(user) }

    it 'assigns a new tournament' do
      get :new

      expect(assigns[:tournament]).to be_new_record
    end
  end

  describe 'POST create' do
    before(:each) { login(user) }

    it 'creates new tournament' do
      expected = expect do
        post :create, tournament: attributes_for(:tournament)
      end

      expected.to change(Tournament, :count).by(1)
    end

    context 'when success' do
      it 'redirects to the tournament path' do
        post :create, tournament: attributes_for(:tournament)

        expect(response).to redirect_to tournament_path(assigns[:tournament])
      end
    end

    context 'when fail' do
      it 'renders :new template' do
        post :create, tournament: attributes_for(:tournament, name: nil)

        expect(response).to render_template :new
      end
    end
  end

  describe 'GET show' do
    let(:tournament) { create(:tournament) }

    it 'assigns an existing tournament' do
      get :show, id: tournament.id

      expect(assigns[:tournament]).to eq tournament
    end
  end

  describe 'GET edit' do
    let(:tournament) { create(:tournament) }

    before(:each) { login(user) }

    it 'assigns an existing tournament' do
      get :edit, id: tournament.id

      expect(assigns[:tournament]).to eq tournament
    end
  end

  describe 'PATCH update' do
    let(:owner)      { create(:management, :owner, manager: user) }
    let(:tournament) { owner.managed_tournament }

    before(:each) do
      login(user)
      request.env['HTTP_REFERER'] = 'http://example.com/where_i_came_from'
    end

    context 'when success' do
      it 'redirects to the tournament path' do
        patch :update,
              id: tournament.id,
              tournament: attributes_for(:tournament, published: true)

        expect(response).to redirect_to tournament_path(tournament)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:owner)       { create(:management, :owner, manager: user) }
    let!(:tournament) { owner.managed_tournament }

    before(:each) { login(user) }

    it 'deletes tournament' do
      expected = expect do
        delete :destroy, id: tournament.id
      end

      expected.to change(Tournament, :count).by(-1)
    end
  end
end
