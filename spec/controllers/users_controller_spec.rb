require 'spec_helper'
require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:user) { create(:user) }

  before :each do
    login(user)
  end

  describe 'GET show' do
    it 'assigns a user as @user' do
      get :show, id: user.id

      expect(assigns[:user]).to eq(user)
    end
  end

  describe 'GET edit' do
    it 'assigns a user as @user' do
      get :edit, id: user.id

      expect(assigns[:user]).to eq(user)
    end
  end

  describe 'PATCH update' do
    before(:each) do
      request.env['HTTP_REFERER'] = 'http://test.host/where_i_came_from'
    end

    it 'redirects to detail view when success' do
      patch :update, id: user.id, user: attributes_for(:user)

      expect(response).to redirect_to user_path(user)
    end
  end
end
