require 'rails_helper'

RSpec.describe ManagementsController, type: :controller do
  describe 'GET index' do
    let(:management) { create(:management) }

    it 'assigns managements of specified tournament' do
      get :index, t_id: management.tournament_id

      expect(assigns[:managements]).to eq [management]
    end
  end

  describe 'GET new' do
    let(:tournament) { create(:tournament) }
    let(:user)       { create(:user) }

    before(:each) { login(user) }

    it 'assigns a new management as @management' do
      get :new, t_id: tournament.id

      expect(assigns[:management]).to be_new_record
    end
  end

  describe 'POST create' do
    let(:tournament) { create(:tournament) }
    let(:user)       { create(:user) }

    before(:each) { login(user) }

    it 'creates new management' do
      expected = expect do
        post :create,
             management: {
               user_id: user.id,
               tournament_id: tournament.id
             }
      end

      expected.to change(Management, :count).by(1)
    end
  end

  describe 'DELETE destroy' do
    let(:management) { create(:management) }
    let(:user)       { management.manager }

    before(:each) { login(user) }

    it 'deletes a management' do
      expected = expect do
        delete :destroy, id: management.id
      end

      expected.to change(Management, :count).by(-1)
    end
  end
end
