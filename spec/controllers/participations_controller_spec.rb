require 'spec_helper'
require 'rails_helper'

RSpec.describe ParticipationsController, type: :controller do
  describe 'GET index' do
    let(:participation) { create(:participation) }

    it 'assigns participations of specified tournament' do
      get :index, t_id: participation.tournament_id

      expect(assigns[:participations]).to eq [participation]
    end
  end

  describe 'GET new' do
    let(:tournament) { create(:tournament) }
    let(:user)       { create(:user) }

    before(:each) { login(user) }

    it 'assigns a new participation as @participation' do
      get :new, t_id: tournament.id

      expect(assigns[:participation]).to be_new_record
    end
  end

  describe 'POST create' do
    let(:tournament) { create(:tournament) }
    let(:user)       { create(:user) }

    before(:each) { login(user) }

    it 'creates new participation' do
      expected = expect do
        post :create,
             participation: {
               user_id: user.id,
               tournament_id: tournament.id
             }
      end

      expected.to change(Participation, :count).by(1)
    end
  end

  describe 'GET edit' do
    let(:participation) { create(:participation) }
    let(:user)          { participation.participant }

    before(:each) do
      login(user)
      get :edit, id: participation.id
    end

    it 'assigns an existing participation' do
      expect(assigns[:participation]).not_to be_new_record
    end

    it 'assigns an specified participation' do
      expect(assigns[:participation]).to eq participation
    end
  end

  describe 'PATCH update' do
    let(:new_name)      { 'new_name' }
    let(:participation) { create(:participation) }
    let(:user)          { participation.participant }

    before(:each) do
      login(user)
      post :update,
           id: participation.id,
           participation: participation.attributes.merge(name: new_name)
    end

    it 'updates an existing participation' do
      expect(assigns[:participation]).not_to be_new_record
    end

    it 'updates an specified participation with new name' do
      expect(assigns[:participation].name).to eq new_name
    end
  end

  describe 'DELETE destroy' do
    let(:participation) { create(:participation) }
    let(:user)          { participation.participant }

    before(:each) { login(user) }

    it 'deletes a participation' do
      expected = expect do
        delete :destroy, id: participation.id
      end

      expected.to change(Participation, :count).by(-1)
    end
  end
end
