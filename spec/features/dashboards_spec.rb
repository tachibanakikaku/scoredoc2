require 'rails_helper'

RSpec.feature 'Dashboards', type: :feature do
  let(:auth_hash)  { OmniAuth.config.mock_auth }
  let(:user_count) { User.count }
  let(:login_user) do
    User.where(
      provider: auth_hash[:twitter][:provider],
      uid: auth_hash[:twitter][:uid]
    ).first
  end

  scenario 'login' do
    visit root_path
    click_link(t('login'))
    expect(page).to have_content t('logout')
    # TODO: next 2 lines are redundancy. want to test `current_user` method
    expect(user_count).to eq 1
    expect(login_user).not_to be_nil
  end
end
