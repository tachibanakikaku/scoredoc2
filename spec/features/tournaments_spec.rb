# coding: utf-8
require 'rails_helper'

feature 'Tournament management' do
  let(:auth_hash)  { OmniAuth.config.mock_auth }
  let(:login_user) do
    User.where(
      provider: auth_hash[:twitter][:provider],
      uid: auth_hash[:twitter][:uid]
    ).first
  end

  scenario 'creates a new tournament', js: :true do
    visit root_path
    expect do
      click_link      t('login')
      click_link      t('view.nav.create_tournament')
      fill_in         'tournament_name',          with: 'Example Tournament'
      select_datetime(DateTime.now,               from: 'tournament_opened_at')
      fill_in         'tournament_description',   with: 'Example Description'
      click_link      t('next')
      check           'acceptTerms'
      click_button    t('create')
      expect(page).to have_content t('view.create_success')
    end
  end
end
