require 'spec_helper'
require 'rails_helper'

RSpec.describe 'tournaments/show', type: :view do
  let(:user)         { create(:user) }
  let(:owner)        { create(:management, :owner, manager: user) }
  let(:tournament)   { create(:tournament) }

  before(:each) do
    login(user)
    assign(:tournament, tournament)

    render
  end

  it 'has a name' do
    expect(rendered).to have_css('td', text: tournament.name)
  end

  it 'has a description' do
    expect(rendered).to have_css('td', text: tournament.description)
  end

  it 'has a opened_at' do
    expect(rendered).to have_css('td', text: tournament.opened_at)
  end

  it 'has a published' do
    expect(rendered).to have_css('td', text: tournament.published)
  end
end
