require 'rails_helper'

RSpec.describe 'tournaments/edit', type: :view do
  let(:tournament) { create(:tournament, published: true) }

  before do
    assign(:tournament, tournament)
    render
  end

  it 'has a name' do
    expect(rendered).to have_xpath("//input[@id='tournament_name'][@value='#{tournament.name}']")
  end

  it 'has a description' do
    expect(rendered).to have_css('textarea#tournament_description', text: tournament.description)
  end

  it 'has a opened_at (year)' do
    expect(rendered).to have_css('select#tournament_opened_at_1i', text: tournament.opened_at.strftime('%Y'))
  end

  it 'has a published' do
    expect(rendered).to have_xpath("//input[@id='tournament_published'][@checked='checked']")
  end
end
