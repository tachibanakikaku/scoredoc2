require 'rails_helper'

RSpec.describe 'tournaments/new', type: :view do
  before do
    assign(:tournament, Tournament.new)
    render
  end

  it 'uses twitter-bootstrap-wizard' do
    expect(rendered).to have_css('div#root-wizard')
  end

  describe 'pagers' do
    it 'has a title with link' do
      expect(rendered).to have_css('li.previous', text: t('previous'))
      expect(rendered).to have_css('li.next', text: t('next'))
    end
  end

  describe 'tabs' do
    it 'has a title with link' do
      expect(rendered).to have_css('li a', text: t('view.tournament.new.basic_info'))
      expect(rendered).to have_css('li a', text: t('view.tournament.new.terms'))
    end
  end

  describe 'fields' do
    it 'has an input field' do
      expect(rendered).to have_field('tournament_name')
      expect(rendered).to have_css('div#tab1 select') # opened_at
      expect(rendered).to have_field('tournament_description')
      expect(rendered).to have_field('acceptTerms')
    end
  end
end
