require 'rails_helper'

RSpec.describe 'dashboard/index', type: :view do
  let(:tournament) { create(:tournament, name: 't1') }

  before do
    assign(:tournaments, [tournament])
    render
  end

  it 'shows tournament with link' do
    expect(rendered).to have_link(tournament.name, href: tournament_path(tournament))
  end
end
