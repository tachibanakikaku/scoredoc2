require 'rails_helper'

RSpec.describe 'users/show', type: :view do
  let(:user) { create(:user) }

  before do
    assign(:user, user)
    render
  end

  describe 'item: name' do
    it 'has a localized label' do
      expect(rendered).to have_css('th', text: t_model(User, :name))
    end

    it 'has a value' do
      expect(rendered).to have_css('td', text: user.name)
    end
  end

  describe 'item: image_url' do
    it 'has a localized label' do
      expect(rendered).to have_css('th', text: t_model(User, :image_url))
    end

    it 'has a image' do
      expect(rendered).to have_css("img[src*='#{user.image_url}']")
    end
  end

  describe 'item: time_zone' do
    it 'has a localized label' do
      expect(rendered).to have_css('th', text: t_model(User, :time_zone))
    end

    it 'has a value' do
      expect(rendered).to have_css('td', text: user.time_zone)
    end
  end
end
