require 'rails_helper'

RSpec.describe 'users/edit', type: :view do
  let(:user) { create(:user) }

  before do
    assign(:user, user)
    render
  end

  it 'has a name' do
    expect(rendered).to have_xpath("//input[@id='user_name'][@value='#{user.name}']")
  end

  it 'has a image_url' do
    expect(rendered).to have_xpath("//input[@id='user_image_url'][@value='#{user.image_url}']")
  end

  it 'has a time_zone' do
    expect(rendered).to have_xpath("//select[@id='user_time_zone']/option[@value = '#{user.time_zone}']")
  end
end
