# Send data to Coveralls
#
# See https://coveralls.io/bitbucket/tachibanakikaku/scoredoc2
require 'simplecov'
require 'coveralls'
require 'capybara/poltergeist'

Capybara.javascript_driver = :poltergeist

Coveralls.wear!('rails')

SimpleCov.formatter = Coveralls::SimpleCov::Formatter
SimpleCov.start do
  add_filter 'vendor/'
  add_filter 'spec/'
end

# The `.rspec` file also contains a few flags that are not defaults but that
# users commonly want.
#
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  require 'factory_girl'
  FactoryGirl.definition_file_paths = Dir['./spec/factories/**/*.rb']
  FactoryGirl.find_definitions

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  Dir['./spec/support/**/*.rb'].sort.each { |f| require f }

  config.include FactoryGirl::Syntax::Methods
  config.include LoginMacros, type: :controller
  config.include LoginMacros, type: :view
  config.include I18nMacros
end

OmniAuth.configure do |config|
  config.test_mode = true
  config.mock_auth[:twitter] = OmniAuth::AuthHash.new(
    provider: 'twitter',
    uid: '123545',
    info: {
      nickname: 'taro',
      name: 'YAMADA Taro',
      location: 'Japan',
      image_url: 'https://example.com/example.png'
    }
  )
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

def select_datetime(date, options = {})
  field = options[:from]

  select date.strftime('%Y'),  from: "#{field}_1i" # year
  select date.strftime('%-m'), from: "#{field}_2i" # month no-padded
  select date.strftime('%-d'), from: "#{field}_3i" # day no-padded
  select date.strftime('%H'),  from: "#{field}_4i" # hour
  select date.strftime('%M'),  from: "#{field}_5i" # minute
end
